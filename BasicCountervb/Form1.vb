﻿Imports BasicCounterLib

Public Class Form1
    Private Sub Btn_moins_Click(sender As Object, e As EventArgs) Handles Btn_moins.Click
        Moncompteur.Moins()
        lbl_nb.Text = Moncompteur.Getnb_compteur()
    End Sub

    Private Sub Btn_plus_Click(sender As Object, e As EventArgs) Handles Btn_plus.Click
        Moncompteur.Plus()
        lbl_nb.Text = Moncompteur.Getnb_compteur()
    End Sub

    Private Sub Btn_raz_Click(sender As Object, e As EventArgs) Handles Btn_raz.Click
        Moncompteur.Raz()
        lbl_nb.Text = Moncompteur.Getnb_compteur()
    End Sub
End Class
