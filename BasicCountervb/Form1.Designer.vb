﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Btn_moins = New System.Windows.Forms.Button()
        Me.Btn_plus = New System.Windows.Forms.Button()
        Me.Btn_raz = New System.Windows.Forms.Button()
        Me.lbl_total = New System.Windows.Forms.Label()
        Me.lbl_nb = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Btn_moins
        '
        Me.Btn_moins.Location = New System.Drawing.Point(49, 117)
        Me.Btn_moins.Name = "Btn_moins"
        Me.Btn_moins.Size = New System.Drawing.Size(80, 49)
        Me.Btn_moins.TabIndex = 0
        Me.Btn_moins.Text = "-"
        Me.Btn_moins.UseVisualStyleBackColor = True
        '
        'Btn_plus
        '
        Me.Btn_plus.Location = New System.Drawing.Point(338, 117)
        Me.Btn_plus.Name = "Btn_plus"
        Me.Btn_plus.Size = New System.Drawing.Size(80, 49)
        Me.Btn_plus.TabIndex = 1
        Me.Btn_plus.Text = "+"
        Me.Btn_plus.UseVisualStyleBackColor = True
        '
        'Btn_raz
        '
        Me.Btn_raz.Location = New System.Drawing.Point(184, 204)
        Me.Btn_raz.Name = "Btn_raz"
        Me.Btn_raz.Size = New System.Drawing.Size(90, 36)
        Me.Btn_raz.TabIndex = 2
        Me.Btn_raz.Text = "RAZ"
        Me.Btn_raz.UseVisualStyleBackColor = True
        '
        'lbl_total
        '
        Me.lbl_total.AutoSize = True
        Me.lbl_total.Location = New System.Drawing.Point(208, 57)
        Me.lbl_total.Name = "lbl_total"
        Me.lbl_total.Size = New System.Drawing.Size(40, 17)
        Me.lbl_total.TabIndex = 3
        Me.lbl_total.Text = "Total"
        '
        'lbl_nb
        '
        Me.lbl_nb.AutoSize = True
        Me.lbl_nb.Location = New System.Drawing.Point(223, 117)
        Me.lbl_nb.Name = "lbl_nb"
        Me.lbl_nb.Size = New System.Drawing.Size(16, 17)
        Me.lbl_nb.TabIndex = 4
        Me.lbl_nb.Text = "0"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CausesValidation = False
        Me.ClientSize = New System.Drawing.Size(495, 276)
        Me.Controls.Add(Me.lbl_nb)
        Me.Controls.Add(Me.lbl_total)
        Me.Controls.Add(Me.Btn_raz)
        Me.Controls.Add(Me.Btn_plus)
        Me.Controls.Add(Me.Btn_moins)
        Me.Name = "Form1"
        Me.Text = "BasicCounter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Btn_moins As Button
    Friend WithEvents Btn_plus As Button
    Friend WithEvents Btn_raz As Button
    Friend WithEvents lbl_total As Label
    Friend WithEvents lbl_nb As Label
End Class
