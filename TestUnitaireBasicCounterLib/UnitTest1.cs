﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasicCounterLib;

namespace TestUnitaireBasicCounterLib
{
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void TestGetterSetter()
        {

            Moncompteur.Setnb_compteur(0);
            Assert.AreEqual(0, Moncompteur.Getnb_compteur());


        }

        [TestMethod]
        public void Testplus()
        {
            Moncompteur.Setnb_compteur(0);
            Moncompteur.Plus();
            Assert.AreEqual(1, Moncompteur.Getnb_compteur());

        }

        [TestMethod]
        public void Testmoins()
        {
            Moncompteur.Setnb_compteur(0);
            Moncompteur.Moins();
            Assert.AreEqual(-1, Moncompteur.Getnb_compteur());

        }

        [TestMethod]
        public void Testraz()
        {

            Moncompteur.Setnb_compteur(42);
            Moncompteur.Raz();
            Assert.AreEqual(0, Moncompteur.Getnb_compteur());

        }

    }
}